ALTER TABLE  db.collage
ADD CONSTRAINT dept_name
        FOREIGN KEY(dept_id)
        REFERENCES db.department(dept_id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE