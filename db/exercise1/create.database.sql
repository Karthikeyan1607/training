CREATE SCHEMA data;
CREATE TABLE  data.school (student_id INT NOT NULL
			 ,name VARCHAR(45) NOT NULL
             ,dob DATE NOT NULL
             ,dept_id INT 
             ,percentage INT NOT NULL
             ,PRIMARY KEY (`student_id`));