/*
 * Requirement:
 *      Write a code to change the number format to Denmark number format .
 *      
 * Entity:
 *      NumberFormatDemo
 *      
 * Function Signature:
 *     public static void main(String[] args) 
 *     
 * Jobs to be Done:
 *      1)Create an object to access the Denmark locale.
 *      2)Get the Denmark number format and store it in numberFormat .
 *      3)Format an number to Denmark format and store it in number of type String .
 *      4)Print the number .
 *      
 * Pseudo code:
 * class NumberFormatDemo {

    public static void main(String[] args) {
        Locale locale = new Locale("da", "DK");
        //Get the specific locale number format
        String number = numberFormat.format(234.25);
        System.out.println(" Denmark number format :" + number);
    }
}
 * 
 */
package com.kpr.training.internationalization;

import java.text.NumberFormat;
import java.util.Locale;

public class NumberFormatDemo {

    public static void main(String[] args) {
        Locale locale = new Locale("da", "DK");
        NumberFormat numberFormat = NumberFormat.getInstance(locale);
        String number = numberFormat.format(234.25);
        System.out.println(" Denmark number format :" + number);
    }
}
