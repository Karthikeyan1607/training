SELECT roll_number
      ,name
      ,dob
      ,gender
      ,email
      ,phone
      ,address
      ,academic_year
      ,semester
      ,gpa
  FROM student
 INNER JOIN semester_result ON semester_result.stud_id = student.id
                            AND (gpa > '8' AND semester = '1');
SELECT roll_number
      ,name
      ,dob
      ,gender
      ,email
      ,phone
      ,address
      ,academic_year
      ,semester
      ,gpa
  FROM student
 INNER JOIN semester_result ON semester_result.stud_id = student.id
                            AND (gpa > '5' AND semester = '1')