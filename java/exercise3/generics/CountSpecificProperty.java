/*
 * Requirements : 
 * 		Write a generic method to count the number of elements in a collection that have a specific
 * property (for example, odd integers, prime numbers, palindromes).
 *
 * Entities :
 * 		CountSpecificProperty.
 * Function Declaration :
 * 		public static int count(ArrayList<Integer> list)
 * 		public static void main(String[] args)
 * Jobs To Be Done:
 * 		1) Create a reference for ArrayList of Integer type.
 * 		2) Add n integers to the ArrayList.
 * 		3) Create a method with ArrayList as argument.
 *			3.1)Initialize a integer variable with 0 as its value.
 *			3.2)Iterate through the list and check each integer is odd.
 *				3.2.1)If the integer is odd then the variable is incremented.
 *			3.3)Return the variable.
 *		4)Print the number of odd numbers in the list using the method.
 *
 * Pseudocode:
 * 
 * Public class CountSpecificProperty {
 * 	  
 * 	   // Creating method to count number of odd numbers in a list.
 *     public static int count(ArrayList<Integer> list) {
 *         int occurence = 0;
 *         for (int elements : list) {
 *             if (elements % 2 != 0) {
 *                 occurence++;
 *             }
 *         }
 *         return occurence;
 *     }
 *     
 *     public static void main(String[] args) {
 *         ArrayList<Integer> list = new ArrayList<>();
 *         //Add 5 integers.
 *         
 *         System.out.println("Number of odd integers : " + count(list));
 */

package com.kpr.training.generics;

import java.util.ArrayList;

public class CountSpecificProperty {

	public static int count(ArrayList<Integer> list) {
		int occurence = 0;
		for (int elements : list) {
			if (elements % 2 != 0) {
				occurence++;
			}
		}
		return occurence;
	}

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(5);
		list.add(4);
		list.add(3);
		list.add(2);
		list.add(1);
		System.out.println("Number of odd integers : " + count(list));
	}
}
