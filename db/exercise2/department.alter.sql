ALTER TABLE training.employee 
ADD COLUMN dept_id INT NULL AFTER annual_salary,
ADD INDEX dept_id_idx (dept_id ASC) ;

ALTER TABLE training.employee 
ADD CONSTRAINT dept_id
  FOREIGN KEY (dept_id)
  REFERENCES training.department (dept_id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
