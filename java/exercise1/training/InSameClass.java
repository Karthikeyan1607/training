package test;

public class InSameClass {

	public static String name;
	private static int age;
	protected static String nationality;

	public static void main(String[] args) {
		name = "Balaji";
		System.out.println(name);

		age = 20;
		System.out.println(age);

		nationality = "Indian";
		System.out.println(nationality);
	}

}
