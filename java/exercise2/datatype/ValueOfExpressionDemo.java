/*
/*Requirement:
    To find the value of the following expression, and to say why
    Integer.valueOf(1).equals(Long.valueOf(1)).

Entity:
    ValueOfExpressionDemo.

Function Declaration:
    public static void main (String[] args).
    valueOf().

Jobs to be Done:
    1. print the given expression.

Answer of the given expression is false. Because the two objects are Integer and Long have different types.
So the given expression is false.
*/

// PROGRAM:
package com.kpr.training.datatype;

public class ValueOfExpressionDemo {

    public static void main(String[] args) {
        System.out.println(Integer.valueOf(1).equals(Long.valueOf(1)));
    }
}
