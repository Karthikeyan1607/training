/*
 * Requirement:
 *    Write a program for email-validation?
 * Entities:
 *    EmailValidation
 * Function Signature:
 *     public static boolean validMail(String email)
 *     public static void main(String[] args)
 * Jobs to be done:
 *      1) Create object for Scanner
 *      2) Get the input as emailId from the user 
 *      3) Create the pattern for the email
 *      4) Check whether the input matches the pattern
 *          4.1) if it matches return true.
 *          4.2) if it not matches return false.
 *      5)Check if returned value is true 
 *         5.1) print entered mailid is valid
 *         5.2) print entered mailid is invalid
 * Pseudocode:
 * class EmailValidation {

    public static boolean validMail(String email) {
        String emailRegex ="Condtion for email validation:;
        //Pattern pattern
         if (email == null)
            return false;
        return pattern.matcher(email).matches();
        }
        public static void main(String[] args) {
        System.out.println("Enter your mailId ");
        Scanner scanner = new Scanner(System.in);
        String email = scanner.next();
        if (validMail(email))
            System.out.print("Entered MailId is valid");
        else
            System.out.print("Entered MailId is invalid");
         }
 }
         
 * 
 */
package com.kpr.training.quantifiers;

import java.util.Scanner;

import java.util.regex.Pattern;

class EmailValidation {

    public static boolean validMail(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." + "[a-zA-Z0-9_+&*-]+)*@"
                + "(?:[a-zA-Z0-9-]+\\.)+[a-z" + "A-Z]{2,7}$";

        Pattern pattern = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pattern.matcher(email).matches();
    }

    public static void main(String[] args) {
        System.out.println("Enter your mailId ");
        Scanner scanner = new Scanner(System.in);
        String email = scanner.next();
        if (validMail(email))
            System.out.print("Entered MailId is valid");
        else
            System.out.print("Entered MailId is invalid");
        scanner.close();
    }
}
