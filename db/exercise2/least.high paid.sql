SELECT dep.dept_name
      ,max(emp.annual_salary) AS highpaid
      ,min(emp.annual_salary) AS leastpaid
 FROM  training.employee emp
	  ,training.department dep 
 WHERE emp.dept_id=dep.dept_id
 GROUP BY dep.dept_id ;