/*
Requirement:
    To create a program that is similar to the previous one but instead of reading integer arguments,
    it reads floating-point arguments.It displays the sum of the arguments, using exactly two digits
    to the right of the decimal point.

Entity:
    FPAdderDemo.

Function Declaration:
    public static void main(String[] args).

Jobs to be Done:
   1. import DecimalFormat from java.text package.
    2. Declare the class FPAdderDemo.
    3. Under a main method declare the variable number1, number2 and number3 of type float and
        convert it to float.
    4. If the length of the arguements is more than two then print the sum of all the arguements
        else show the error message.
    5. Now format the output value.
*/
package com.kpr.training.datatype;

import java.text.DecimalFormat;

public class FPAdderDemo {
    
    public static void main(String[] args) {
        float number1 = Float.parseFloat(args[0]);
        float number2 = Float.parseFloat(args[1]);
        float number3 = Float.parseFloat(args[2]);
        if (args.length < 2) {
            System.out.println("Error");
        } else {
        double sum = 0.0;

        for (int i = 0; i < args.length; i++) {
                sum += Double.valueOf(args[i]).doubleValue();
        }

        DecimalFormat myFormatter = new DecimalFormat("###,###.##");
        String outputValue = myFormatter.format(sum);
            System.out.println(outputValue);
        }
    }
}
