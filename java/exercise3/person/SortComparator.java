/*
* Requirement:
*      sort the roster list based on the person's age in descending order using comparator
* Entity:
*      SortComparator
* Function Signature :
*      public static void main(String[] args)
* Jobs to be done:
*      1)Invoke a method createRoster from the class Person and store it to the List.
*      2)Sort the roster list in descending order using comparator .
*      3)For each elements in roster list .
*           3.1)Print the element .
*      
* Pseudo code:
* 
* class SortComparator {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        //sort the roster using comparator
        roster.sort(Comparator.comparing(Person::getAge));
        Stream<Person> stream = roster.stream();
        stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));
    }
}

* 
*/
package com.kpr.training.person;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class SortComparator {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        roster.sort(Comparator.comparing(Person::getAge));
        Stream<Person> stream = roster.stream();
        stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));
    }
}
