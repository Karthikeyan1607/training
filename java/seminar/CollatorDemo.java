package mypackage;

import java.text.Collator;
import java.util.Locale;

public class CollatorDemo {
    public static void main(String[] args) {

        Locale locale = Locale.UK;
        // this class to build searching and sorting routines for natural language text.
        Collator collator = Collator.getInstance(locale);
        int result = collator.compare("cat", "Animal");
        System.out.print(result);
    }
}
