/* Requirement:
 * To find the output of the code and complete the code.
 *     	Queue bike = new PriorityQueue();    
        bike.poll();
        System.out.println(bike.peek()); 
           
 *Entity:
 *   QueueDemo
 *   
 *Method Signature:
 *   public static void main(String[] args)
 *   
 * Jobs to be done:
 *    1.The given program is incomplete without the class declaration.
 *    2.The class and main function should be declare.
 *    3.By running the program, we can find the output of the program.
 * 
 *Pseudo Code:
 *		class QueueDemo {
 *			public static void main(String[] args) {
 *				Queue bike = new PriorityQueue();    
        		bike.poll();
        		System.out.println(bike.peek());
 *			}
 *		}
 */
//Completed Code:
package com.kpr.training.stack_queue;

import java.util.PriorityQueue;
import java.util.Queue;

public class QueueDemo {

	public static void main(String[] args) {
		Queue bike = new PriorityQueue();
		bike.poll();
		System.out.println(bike.peek()); // Prints null
      
	}

}
//output of the code: null