CREATE TABLE training.department(
  dept_id INT NOT NULL,
  dept_name VARCHAR(45) NOT NULL,
  PRIMARY KEY (`dept_id`));