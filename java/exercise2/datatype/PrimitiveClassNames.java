/*
Requirement:
    To print the classname of all the primitive data types.

Entity:
    PrimitiveClassNames
    
Function Signature:
    public static void main(String[] args)
     
Jobs To Be Done:
    1.Print the respective classname of primitive datatype.
    
*/
package com.kpr.training.datatype;

public class PrimitiveClassNames {
    public static void main(String[] args) {
        String intClassName = int.class.getName();
        System.out.println("Class Name of Int : " + intClassName);

        String charClassName = char.class.getName();
        System.out.println("Class Name of Char : " + charClassName);

        String doubleClassName = double.class.getName();
        System.out.println("Class Name of double : " + doubleClassName);

        String floatClassName = float.class.getName();
        System.out.println("Class Name of float : " + floatClassName);
    }
}
