package mypackage;

import java.text.ParseException;
import java.text.RuleBasedCollator;

public class GroupingCollation {
    public static void main(String[] args) throws ParseException {

        String rules = "< c < ch < a < b";

        RuleBasedCollator ruleBasedCollator = new RuleBasedCollator(rules);

        int result = ruleBasedCollator.compare("cat", "chess");
        System.out.println(result);

        result = ruleBasedCollator.compare("boss", "charlie");
        System.out.println(result);
    }
}
