/*
 * Requirements : 
 * 		To explain the working of contains(), isEmpty() and give example.
 * 
 * Entities :
 * 		SetExample.
 * 
 * Method Signature :
 * 		public static void main(String[] args)
 * 
 * Jobs To Be Done:
 * 		1)Create an object for HashSet with Integer type.
 *      2)Add the elements to the set.
 *      3)Check Whether the specific element is present in the set.
 *          3.1)If the element is present, Print true.
 *          3.2)Otherwise, Print false.
 *      4)Check Whether the set is empty,
 *          4.1)If it is empty, Print true.
 *          4.2)Otherwise, Print false .
 *          
 * Pseudo code:
 * class SetExample {

    public static void main(String[] args) {
        HashSet<Integer> set = new HashSet<>();
        //Add elements to the set
        System.out.println(set.contains(element)); // returns true
        System.out.println(set.isEmpty()); // returns false
    }

}
 * Explanation:
 * 	contains(<T>):
 * 		returns true if the element is present in the list else returns false.
 * 	isEmpty(<T>):
 * 		returns true if the set is empty else returns false.
 */
package com.kpr.training.list_and_set;

import java.util.HashSet;

public class SetExample {

	public static void main(String[] args) {
		HashSet<Integer> set = new HashSet<>();
		set.add(12);
		set.add(30);
		set.add(500);
		System.out.println(set.contains(500)); // returns true
		System.out.println(set.isEmpty()); // returns false
	}

}
