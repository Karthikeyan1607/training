package mypackage;

import java.lang.Character;
import java.util.Scanner;

public class IsDefined {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a character ");
        char character1 = sc.next().charAt(0);
        // character is given with a defined meaning in the Unicode
        boolean boolean1 = Character.isDefined(character1); // returns true or false
        System.out.println(boolean1);
    }
}
