/*
 * Requirement:
 *      List<Person> newRoster = new ArrayList<>();
            newRoster.add(
                new Person(
                "John",
                IsoChronology.INSTANCE.date(1980, 6, 20),
                Person.Sex.MALE,
                "john@example.com"));
            newRoster.add(
                new Person(
                "Jade",
                IsoChronology.INSTANCE.date(1990, 7, 15),
                Person.Sex.FEMALE, "jade@example.com"));
            newRoster.add(
                new Person(
                "Donald",
                IsoChronology.INSTANCE.date(1991, 8, 13),
                Person.Sex.MALE, "donald@example.com"));
            newRoster.add(
                new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
        - Create the roster from the Person class and add each person in the newRoster to the existing list and print the new roster List.
        - Print the number of persons in roster List after the above addition.
        - Remove the all the person in the roster list
        
 * Entity:
      AddPerson
  
 * Function Signature:
      public static void main(String[] args) 
  
 * Jobs to be done:
 *    1) Invoke a method createRoster from the class Person and store it to the List.
 *    2) Create a object for a newRoster list with type Person.
 *        2.1) Add a given names in the list.
 *    3) Add all the names in the newRoster list to the roster list.
 *    4) For each person in the roster list print the person.
 *    5) Print the size of roster list.
 *    6) Clear the roster list.
 *    7) Print the roster list.
 *    
 * Pseudo Code:
 * class AddPerson {

    public static void main(String[] args) {
        // Referring from Person.java
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = new ArrayList<>();
        //Add the new names to the new list
        
        //Add all the names from new list to predefined list
        roster.addAll(newRoster);
        
        // Stream is created
        // Using Stream
        // list elements are printed using stream
        System.out.println("Number of person in the roster list :" + roster.size());
        
        // clear the list
        roster.clear();
        System.out.println("After removing all elements frfom the list :" + roster);
    }

}        
         
*/
package com.kpr.training.person;

import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class AddPerson {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = new ArrayList<>();
        newRoster.add(new Person("John", IsoChronology.INSTANCE.date(1980, 6, 20), Person.Sex.MALE,
                "john@example.com"));
        newRoster.add(new Person("Jade", IsoChronology.INSTANCE.date(1990, 7, 15),
                Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(new Person("Donald", IsoChronology.INSTANCE.date(1991, 8, 13),
                Person.Sex.MALE, "donald@example.com"));
        newRoster.add(new Person("Bob", IsoChronology.INSTANCE.date(2000, 9, 12), Person.Sex.MALE,
                "bob@example.com"));
        roster.addAll(newRoster);
        Stream<Person> stream = roster.stream();
        stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));
        System.out.println("Number of person in the roster list :" + roster.size());
        roster.clear();
        System.out.println("After removing all elements frfom the list :" + roster);
    }

}