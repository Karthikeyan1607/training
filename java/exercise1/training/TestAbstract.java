package test;

public class TestAbstract extends Abstract {

	public void printName() {
		System.out.println("Balaji");
	}

	private void printAge() {
		System.out.println("20");
	}

	protected void printNationality() {
		System.out.println("Indian");
	}

	public static void main(String[] args) {
		TestAbstract test = new TestAbstract();
		test.printName();
		test.printAge();
		test.printNationality();

	}
}
