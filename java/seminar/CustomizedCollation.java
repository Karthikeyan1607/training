package mypackage;

// Customized Collation Rules
import java.text.ParseException;
import java.text.RuleBasedCollator;

public class CustomizedCollation {
    public static void main(String[] args) throws ParseException {

        String rules = "< c < d < a < b";

        RuleBasedCollator ruleBasedCollator = new RuleBasedCollator(rules);

        int result = ruleBasedCollator.compare("chess", "barol");
        System.out.println(result);
    }
}
