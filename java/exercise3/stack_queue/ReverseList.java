/*
 * Requirements : 
 * 		Reverse List Using Stack with minimum 7 elements in list.
 * 
 * Entities :
 * 		public class ReverseList.
 * 
 * Method Signature :
 * 		public static void main(String[] args)
 * 
 * Jobs To Be Done:
 * 		1)Create an object for ArrayList with type as Integer.
 *      2)Add the elements to the list.
 *      3)Print the elements of list.
 *      4)Create an object for Stack with type as Integer.
 *      5)for each element,
 *      	5.1)Push to the stack
 *      6)Clear the list.
 *      7)Create a variable size with Integer type and store it in stack size.
 *      8)for each element,
 *      	8.1)Poped from stack, till the size of the stack and poped elements are add to the list.
 *      9)Print the elements of the list.  
 * Pseudo Code:
 * 		class ReverseList {
 * 			public static void main(String[] args) {
 * 				//Create the array list
 * 				ArrayList<Integer> list = new ArrayList<>();
 * 
 * 				//Add some elements to the list using the given syntax
 * 				list.add(5);
 * 
 * 				//create a stack 
 * 				Stack<Integer> stack = new Stack<>();
 * 
 * 				//push the elements from the list in the stack
 * 				for(Integer i : list) {
 * 					stack.push(i);
 * 				} 
 * 				
 * 				//clear the list elements and make it as empty.
 * 				for(int i = 0; i < stack.size(); i++) {
 * 					//append the stack elements to the list
 * 				}
 * 				System.out.println(list)
 * 			}
 * 		}
 * 
 */
package com.kpr.training.stack_queue;

import java.util.ArrayList;
import java.util.Stack;

public class ReverseList {

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(5);
		list.add(4);
		list.add(3);
		list.add(2);
		list.add(1);
		list.add(0);
		list.add(-1);
		System.out.println("Before Reversing : " + list);

		Stack<Integer> stack = new Stack<>();
		for (Integer value : list) {
			stack.push(value);
		}
		list.clear();
		int size = stack.size();
		for (int iteration = 0; iteration < size; iteration++) {
			list.add(stack.pop());
		}
		System.out.println("After Reversing : " + list);

	}
}
