package mypackage;

import java.lang.Character;
import java.util.Scanner;

public class GetType {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a character ");
        char character1 = sc.next().charAt(0);
        int int1 = Character.getType(character1);
        // returns a value indicating a character's general category
        System.out.println(int1);
        if (int1 == Character.LOWERCASE_LETTER) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }
}
