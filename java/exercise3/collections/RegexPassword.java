/*
 * Requirement:
 *       create a pattern for password which contains
 *       8 to 15 characters in length
 *       Must have at least one uppercase letter
 *       Must have at least one lower case letter
 *       Must have at least one digit
 *Entity:
 *     RegexPassword
 * Function Declaration:
 *      public static boolean ValidPassword(String password);
 *      public static void main(String[] args)
 * Jobs to Done:
 *      1) Get the input from the user 
 *      2) Create the pattern for the password
 *      3) Check whether the input matches the pattern
 *          3.1) if it matches return true.
 *          3.2) if it not matches return false.
 * Pseudocode:
 *     
    public class RegexPassword {

      public static boolean ValidPassword(String password) {
          String regex = "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + "(?=\\S+$).{8,15}$";
          //Pattern pattern or String pattern
          if (input != pattern)
              return false;
          else
              return true;
          }
          public static void main(String[] args) {
          Scanner scanner = new Scanner(System.in);
          String password = scanner.next();
          System.out.println(ValidPassword(password));
          }
    }

 */
package com.kpr.training.collections;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexPassword {

    public static boolean ValidPassword(String password) {
        String regex = "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + "(?=\\S+$).{8,15}$";

        Pattern pattern = Pattern.compile(regex);
        if (password == null) {
            return false;
        }
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the Password ");
        String password = scanner.next();
        System.out.println(ValidPassword(password));
        scanner.close();
    }
}
