SELECT emp.first_name
      ,emp.area AS same_area
	  ,dep.dept_name AS same_dept
  FROM training.employee emp,training.department dep 
  WHERE emp.dept_id = dep.dept_id
  AND emp.area IN('Chennai')
  AND dep.dept_name IN('Engineering');
  