/*
 * Requirement:
 *   To Create a queue using generic type and in both implementationPriority Queue, linked list and complete the following.
 *   	1)add atleast 5 elements.
 *   	2)remove the front element
 *   	3)search a element in stack using contains keyword and print boobelan value.
 *   	4)print the size of stack.
 *   	5)print the elements using steam.
 *   
  Entity:
     QueueGeneric
      
  Method Signature:
     public static void main(String[] args)
  
  Jobs to be done:
     1)Create a reference for LinkedList with type queue with generic type Integer.
     2)Add the elements to the queue and print the queue elements.
     2)Remove the front element in the queue and print the queue.
     3)Check Whether the queue contains the specific element
     	3.1)If element is present, Print true.
     	3.2)Otherwise Print false.
     4)Print the size of the queue.
     5)for each element,
        5.1)Print the queue elements.
     6)Create a reference for PriorityQueue with type queue with generic type Integer.
     7)Add the elements to the new queue and print the new queue elements.
     8)Remove the front element in the new queue and print the new queue elements.
    9)Check Whether the new queue contains the specific element
     	9.1)If element is present, Print true.
     	9.2)Otherwise, Print false.
    10)Print the size of the new queue.
    11)for each element,
    	11.1)Print the new queue elements.
       
      
   Pseudo Code:
   		class QueueGeneric {
   			public static void main(String[] args) {
   				//create the queue and queue1 for linked list and priority queue
   				queue/queue1.add(Some elements);
   				System.out.println(queue/queue1.remove());
   				boolean element = queue/queue1.contain(any element);
   				//Create stream for the queue/queue1
   				//printing the queue/queue1 elements using stream in foreach  
   				
   			}   				
      
 */
package com.kpr.training.stack_queue;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Stream;

public class QueueGeneric {

	public static void main(String[] args) {
		System.out.println("Using LinkedList");
		Queue<Integer> queue = new LinkedList<>();
		queue.add(1);
		queue.add(2);
		queue.add(3);
		queue.add(4);
		queue.add(5);
		System.out.println("The queue elements are ");
		System.out.println(queue);
		// Removing the front element
		System.out.println("Removing the front element " + queue.remove());
		System.out.println("After removing the first element " + queue);
		// Check if Queue Contains Element
		boolean element = queue.contains(3);
		System.out.println(element);
		System.out.println("The size of the queue is " + queue.size());
		Stream<Integer> stream = queue.stream();
		System.out.println("The queue element are printed by using stream");
		stream.forEach(elements -> System.out.print(elements + " "));

		System.out.println("Using Priority Queue");
		Queue<Integer> queue1 = new PriorityQueue<>();
		queue1.add(1);
		queue1.add(2);
		queue1.add(3);
		queue1.add(4);
		queue1.add(5);
		System.out.println("The queue elements are ");
		System.out.println(queue1);
		// Removing the front element
		System.out.println("Removing the front element " + queue1.remove());
		System.out.println("After removing the first element " + queue1);
		// Check if Queue Contains Element
		boolean element1 = queue1.contains(3);
		System.out.println(element1);
		System.out.println("The size of the queue is " + queue1.size());
		Stream<Integer> stream1 = queue1.stream();
		System.out.println("The queue element are printed by using stream");
		stream1.forEach(elements -> System.out.print(elements + " "));

	}

}