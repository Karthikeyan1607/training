/*
 * Requirement:
 * 		To demonstrate the abstract class using the class shape that has two methods to
 * calculate the area and perimeter of two classes named the Square and the Circle extended from the
 * class Shape.
 * 
 * Entity:
 *		AbstractDemo
 * 
 * Method Signature:
 * 		public static void main(String[] args)
 * 
 * Jobs To Be Done:
 * 		1)Create a Scanner object.
 * 		2)get two double inputs from the user using scanner object.
 * 		3)Create a object for Square class and pass the length value in the constructor.
 * 		4)Invoke the printArea and printPerimeter method.
 * 		5)Create a object for Circle class and pass the radius value in the constructor.
 * 		6)Invoke the printArea and printPerimeter method.
 */
package com.kpr.training.abstractDemo;

import java.util.Scanner;

public class AbstractDemo {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		double sideLength = scanner.nextDouble();
		double radius = scanner.nextDouble();
		Square square = new Square(sideLength);
		square.printArea();
		square.printPerimeter();
		Circle circle = new Circle(radius);
		circle.printArea();
		circle.printPerimeter();
	}
}