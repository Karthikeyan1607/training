/*
Requirement:
    To demonstrate the method overloading using varArgs.

Entity:
    VarArgDemo

Function Signature:
    public static void main(String[] args).
    public void argsment(int ... numbers)
    public void argsment(String message, int numnbers).

Jobs to be done:
    1) Create a object for the class.
    2) Call the method by passing parameter .
        2.1)print the length .
        2.2)Print the elements using Iteration.
*/

package com.kpr.training.variable_arguments;

public class VarArgDemo {
    public void argument(int... numbers) {
        System.out.println("Number of intger values: " + " " + numbers.length + "The numbers: ");
        for (int values : numbers) {
            System.out.println("The values are: " + " " + values);
        }
    }

    public void argument(String message, int... numbers) {
        System.out.println(message + " the number of values" + " " + numbers.length);
        for (int Values : numbers) {
            System.out.println(" The integer values are " + " " + Values + " ");
        }
    }

    public static void main(String[] args) {
        VarArgDemo variable = new VarArgDemo();
        variable.argument("The message is deleted", 50, 100, 150, 200);
    }
}
