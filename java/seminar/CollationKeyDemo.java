package mypackage;

import java.text.RuleBasedCollator;
import java.text.ParseException;
import java.util.Arrays;
import java.text.CollationKey;

public class CollationKeyDemo {
    public static void main(String[] args) throws ParseException {

        String rules = "< c,C < b,B < a,A";

        RuleBasedCollator ruleBasedCollator = new RuleBasedCollator(rules);

        CollationKey[] collationKeys = new CollationKey[3]; // represents a String under the rules
                                                            // of a specific Collator object.

        collationKeys[0] = ruleBasedCollator.getCollationKey("boss");
        collationKeys[1] = ruleBasedCollator.getCollationKey("carol");
        collationKeys[2] = ruleBasedCollator.getCollationKey("andy");

        Arrays.sort(collationKeys);

        for (CollationKey collationKey : collationKeys) {
            System.out.println(collationKey.getSourceString());
        }
    }
}
