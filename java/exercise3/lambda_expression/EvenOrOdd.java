/*
 * Requirements : 
 * 		 To convert the following anonymous class into lambda expression.
 *  interface CheckNumber {
	    public boolean isEven(int value);
	}
	
	public class EvenOrOdd {
	    public static void main(String[] args) {
	        CheckNumber number = new CheckNumber() {
	            public boolean isEven(int value) {
	                if (value % 2 == 0) {
	                    return true;
	                } else return false;
	            }
	        };
	        System.out.println(number.isEven(11));
	    }
	}
 * Entities :
 * 		CheckNumber
 * 		EvenOrOdd
 * Method Signature :
 * 		boolean isEven(int number);
 * 		public static void main(String[] args).
 * Jobs To Be Done:
 * 		1)Create a Scanner object.
 * 		2)Initialize the integer variable with input value from user.
 * 		3)Define a lambda expression.
 * 		4)check the number is even or odd.
 * 			4.1)if true return true.
 * 			4.2)else return false.
 * 		5)Print the result.
 * 
 * Pseudocode:
 * 
 * interface CheckNumber {
 *     boolean isEven(int number);
 * }
 * public class EvenOrOdd{
 *     public static void main(String[] args) {
 *         Scanner scanner = new Scanner(System.in);
 *         int numberInput = scanner.nextInt();
 *         CheckNumber check = (number) -> (number % 2 ==0) ? true : false;
 *         System.out.println(check.isEven(numberInput));
 *     }
 * }
 */
package com.kpr.training.lambda_expression;

import java.util.Scanner;

interface CheckNumber {

	boolean isEven(int number);
}

public class EvenOrOdd {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		int numberInput = scanner.nextInt();
		CheckNumber check = (number) -> (number % 2 == 0) ? true : false;
		System.out.println(check.isEven(numberInput));
	}
}