SELECT emp1.first_name AS Name1, emp2.first_name AS Name2, emp1.area
FROM training.employee emp1, training.employee emp2
WHERE emp1.dept_id = emp2.dept_id
AND emp1.area = emp2.area
