/*
 * Requirements:
 *    To demonstrate insertions and string buffer tin tree set. 
 *    
 * Entities:
 *     StringBufferDemo 
 *     
 * Method signature:
 *     public static void main(String[] args)
 *     
 * Jobs To Done:
 *      1) Create treeset reference with Stringbuffer.
 *      2) Add the values in treeset.
 *      3) print the treeset.
 *      4) Create method to compare two string.
 * PseudoCode:
 * 
 * public class StringBufferDemo {
    
    public static void main(String[] args) {
        TreeSet<StringBuffer> treeset = new TreeSet<StringBuffer>(new StringComp());
        //Add the values in the treeset
        System.out.println(treeset);
        }
    }
    class StringCompare implements Comparator<StringBuffer> {
        public int compare(StringBuffer string1, StringBuffer string2) {
            //compare the two strings and return it.
        }
     }
    
 */
package com.kpr.training.collections;

import java.util.Comparator;
import java.util.TreeSet;

public class StringBufferDemo {

    public static void main(String[] args) {
        TreeSet<StringBuffer> treeset = new TreeSet<StringBuffer>(new StringCompare());
        treeset.add(new StringBuffer("Sagar"));
        treeset.add(new StringBuffer("Dharmesh"));
        treeset.add(new StringBuffer("Sujeet"));
        treeset.add(new StringBuffer("Rajesh"));
        System.out.println(treeset);

    }
}


class StringCompare implements Comparator<StringBuffer> {
    public int compare(StringBuffer string1, StringBuffer string2) {
        String value1 = string1.toString();
        String value2 = string2.toString();
        return value1.compareTo(value2);
    }
}
/*
 * it is important to have Comparator implementation because StringBuffer doesnot implements
  compareTo() method
 */
