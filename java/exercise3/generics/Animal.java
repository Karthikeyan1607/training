/*
 * Requirements: To write a program to demonstrate generics - class objects as type literals.
 *
 * Entities: Animal
 * 
 * Method Signature: public void sound()
 * 
 * Jobs To Be Done: 1. Declare a method.
 * 
 * Pseudo code: 
 * 		interface Animal {
 * 
 * 			public void sound(); 
 * 		}
 */
package com.kpr.training.generics;

interface Animal {

    public void sound();
}
