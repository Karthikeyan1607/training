/*
Requirement:
    To print the sum of the variable arguments.

Entity:
    public class LambdaVariableArguments

Function Signature:
    public int printAddition(int ... numbers)
    public static void main(String[] args)
    
Jobs to be done:
     1) Declare a lambda expression which takes integer array as argument.
     	1.1)Initialize the integer variable with value as 0.
     	1.2)Iterate the array using for loop.
     	1.3)Add each element to the variable.
     	1.4)return the sum.
     2) Print the resultant sum.
Pseudo code:
interface VariableArgumentDemo {

    public int printAddition(int... numbers);
}


class LambdaVariableArguments {

    public static void main(String[] args) {
        VariableArgumentDemo value = (numbers) -> {
            int sum = 0;
            //Iterate and add all the elements
            for (int values : numbers) {
                sum = sum + values;
            }

            return sum;
        };

        System.out.println("The sum of elements is: " + value.printAddition(elements));
    }
}


 */
package com.kpr.training.lambda_expression;

interface VariableArgumentDemo {

	public int printAddition(int... numbers);
}


public class LambdaVariableArguments {

	public static void main(String[] args) {
		VariableArgumentDemo value = (numbers) -> {
			int sum = 0;

			for (int values : numbers) {
				sum = sum + values;
			}

			return sum;
		};

		System.out.println("The sum of elements is: " + value.printAddition(1, 5, 6, 7));
	}
}