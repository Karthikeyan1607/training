/*Requirements:
 *      To write a program to print employees name list by implementing iterable interface.
 * 
 * Entity:
 *      MyIterable, IterableInterface
 * 
 * Method Signature:
 *      public MyIterable(T[] t),
 *      public Iterator<T> iterator().
 * 
 * Jobs To Be Done:
 *      1)Create a String list and assign some values to it.
 *      2)Create a Iterable interface 
 *      3)Print the  list values.
 * 
 * Pseudo Code:
 *      class MyIterable<T> implements Iterable<T> {
 *          //declare a local variable called as list
 * 
 *          //declare a method declaration that adds the employee name to the local variable list.
 *          MyIterable(T[] t) {
 *              list = Arrays.asList(t);
 *          }
 * 
 *          //return the list that has been iterated.
 *      }
 *      
 *      class IterableIInterface {
 *          public static void main(String[] args) {
 *              //creating the array list with string type
 *              String[] name = {"Add some names to the list"};
 * 
 *              //calling the MyIterable method to print it in Iterable interface.
 *              MyIterable<String> myList = new MyIterable<>(name);
 *          
 *              //printing the employee name that is iterated and returned from another class.
 *              for (String i : myList) {
 *                  System.out.println(i);
 *              }
 * 
 *          }
 *      }
 */ 

package com.kpr.training.generics;

import java.util.List;
import java.util.Arrays;
import java.util.Iterator;

class MyIterable<T> implements Iterable<T> {

    private List<T> list;

    public MyIterable(T[] t) {

        list = Arrays.asList(t);

    }


    public Iterator<T> iterator() {
        return list.iterator();
    }


 public static void main(String[] args) {

        String[] name = {"Balaji Pa", "Boobalan P", "Gokul D", "Karthikeyan C", "Kiruthic P"};
        MyIterable<String> myList = new MyIterable<>(name);

        for (String i : myList) {

            System.out.println(i);
        }
    }
}