package com.kpr.training.date_and_time;

/*
Requirement:
    To display the default time zone and Any three particular time zone.
     
Entity:
    TimeZoneDemo
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. Set the default time zone to GMT.
    2. Create an instance for SimpleDateFormat as daeFormatand store the default date format.
    3. Create an instance for Date as date
    4. Format the date as dateFormat and store it in the String variable currentDateTime
    5. Print the currentDateTime.
    6. Get the different time zones and print that time zone.
    
Pseudo code:
class TimeZoneDemo {
    
    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy EEEE hh:mm:ss a");
        Date date = new Date();
        String currentDateTime = dateFormat.format(date);
        System.out.println("GMT-" + currentDateTime);
        
        TimeZone timeZone = TimeZone.getTimeZone("Europe/Copenhagen");
        dateFormat.setTimeZone(timeZone);
        currentDateTime = dateFormat.format(date);
        System.out.println("Europe/Copenhagen-" + currentDateTime);
        
        timeZone.getTimeZone("America/Managua");
        dateFormat.setTimeZone(timeZone);
        currentDateTime = dateFormat.format(date);
        System.out.println("America/Managua-" + currentDateTime);
        
        timeZone.getTimeZone("Pacific/Auckland");
        dateFormat.setTimeZone(timeZone);
        currentDateTime = dateFormat.format(date);
        System.out.println("Pacific/Auckland-" + currentDateTime);
        
        
    }
}
 */
import java.util.Date;
import java.util.TimeZone;
import java.text.SimpleDateFormat;

public class TimeZoneDemo {
    
    @SuppressWarnings("static-access")
    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy EEEE hh:mm:ss a");
        Date date = new Date();
        String currentDateTime = dateFormat.format(date);
        System.out.println("GMT-" + currentDateTime);
        
        TimeZone timeZone = TimeZone.getTimeZone("Europe/Copenhagen");
        dateFormat.setTimeZone(timeZone);
        currentDateTime = dateFormat.format(date);
        System.out.println("Europe/Copenhagen-" + currentDateTime);
        
        timeZone.getTimeZone("America/Managua");
        dateFormat.setTimeZone(timeZone);
        currentDateTime = dateFormat.format(date);
        System.out.println("America/Managua-" + currentDateTime);
        
        timeZone.getTimeZone("Pacific/Auckland");
        dateFormat.setTimeZone(timeZone);
        currentDateTime = dateFormat.format(date);
        System.out.println("Pacific/Auckland-" + currentDateTime);
        
        
    }
}