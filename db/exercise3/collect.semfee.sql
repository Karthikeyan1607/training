SELECT college.name
      ,semester
      ,paid_status
      ,SUM(amount) AS Total_Amount
      ,paid_year
  FROM semester_fee
 INNER JOIN college_department
        ON semester_fee.cdept_id = college_department.cdept_id
 INNER JOIN college
        ON college.id = college_department.college_id
 WHERE paid_year = 2020
 GROUP BY college.name
         ,semester_fee.semester
         ,semester_fee.paid_status;
SELECT semester
      ,paid_status
      ,SUM(amount) AS Total_Amount
      ,paid_year
 FROM semester_fee
 WHERE paid_status = "PAID" 
  AND  paid_year =2020
GROUP BY semester 
        ,paid_status;