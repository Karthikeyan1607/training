SELECT university.university_name
  ,student.id AS roll_number
  ,student.name AS student_name
  ,student.gender
  ,student.dob
  ,student.address
  ,college.name AS college_name
  ,department.dept_name
  ,semester_fee.amount
  ,semester_fee.paid_year
  ,semester_fee.paid_status
 FROM university university
  ,college college
  ,department department 
  ,college_department college_department
  ,student student
  ,semester_fee semester_fee
 WHERE c.univ_code = u.univ_code 
 AND u.univ_code = d.univ_code 
 AND cd.college_id = c.id 
 AND cd.udept_code = d.dept_code
 AND s.college_id = c.id
 AND s.cdept_id = cd.cdept_id
 AND sy.cdept_id = cd.cdept_id
 AND sf.stud_id = s.id
 AND sf.cdept_id = cd.cdept_id
 ORDER BY roll_number ;