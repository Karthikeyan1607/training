CREATE VIEW crossjoin AS
SELECT collage.student_id
      ,collage.name
      ,collage.percentage
      ,department.dept_name
FROM  db.collage
CROSS JOIN db.department;

SHOW TABLES;
