SELECT college.student_id
	  ,college.name
      ,college.dob
      ,college.percentage
      ,department.dept_name
 FROM data.college
 CROSS JOIN data.department;
 
SELECT college.student_id
	  ,college.name
      ,college.dob
      ,college.percentage
      ,department.dept_name
 FROM data.college
 INNER JOIN data.department
         ON data.college.dept_id = data.department.dept_id;
         
SELECT college.student_id
	  ,college.name
      ,college.dob
      ,college.percentage
      ,department.dept_name
 FROM data.college
  LEFT JOIN data.department
         ON data.college.dept_id = data.department.dept_id;
         
SELECT college.student_id
	  ,college.name
      ,college.dob
      ,college.percentage
      ,department.dept_name
 FROM data.college
 RIGHT JOIN data.department
         ON data.college.dept_id = data.department.dept_id;  